Hi! This is a repository to collect ideas for new spinners.

# I have an awesome idea!

Awesome! Submit it [here](https://gitlab.com/gitmate/open-source/gitmate-spinner-ideas/issues/new)!

Please ask at least 5 of your friends to vote for the sentences they like best.

**NOTE:** they cannot vote for all your ideas or our bot will disqualify their votes. Votes work like priorities - not everything can have a high priority.

# I want to vote!

Splendid! Go to the [issue list](https://gitlab.com/gitmate/open-source/gitmate-spinner-ideas/issues) and vote. Be sure to not just vote for all issues of one person, see above.

** Don't vote for yourselves! **